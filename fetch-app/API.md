openapi: 3.1.0
info:
  title: FetchApp
  version: '1.0'
  summary: Simple Api
  description: Simple Api fetchApp
servers:
- url: http://localhost:3000
paths:
  "/api/claims":
    get:
      summary: Get user data
      tags: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors: {}
                    required:
                    - code
                  data:
                    type: object
                    properties:
                      name:
                        type: string
                        minLength: 1
                      role:
                        type: string
                        minLength: 1
                      phone:
                        type: string
                        minLength: 1
                      created_at:
                        type: string
                        minLength: 1
                    required:
                    - name
                    - role
                    - phone
                    - created_at
                required:
                - status
                - meta
                - data
                x-examples:
                  example-1:
                    status: true
                    meta:
                      code: 200
                      errors:
                    data:
                      name: micheal
                      role: user
                      phone: '0813277445'
                      created_at: 11 Nov 411 11:00 WIB
              examples:
                example:
                  value:
                    status: true
                    meta:
                      code: 200
                      errors:
                    data:
                      name: micheal
                      role: user
                      phone: '0813277445'
                      created_at: 11 Nov 411 11:00 WIB
        '500':
          description: Internal Server Error
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors:
                        type: string
                        minLength: 1
                    required:
                    - code
                    - errors
                  data: {}
                required:
                - status
                - meta
                x-examples:
                  example-1:
                    status: false
                    meta:
                      code: 401
                      errors: Unauthorization
                    data:
              examples:
                example:
                  value:
                    status: false
                    meta:
                      code: 401
                      errors: Unauthorization
                    data:
      operationId: get api claim
      description: Get data from jwt token
      parameters: []
      security:
      - bearerAuth: []
  "/api/comodity":
    get:
      summary: get commodities
      tags: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors: {}
                    required:
                    - code
                  data:
                    type: array
                    uniqueItems: true
                    minItems: 1
                    items:
                      required:
                      - uuid
                      - area_provinsi
                      - area_kota
                      - size
                      - price
                      - tgl_parsed
                      - timestamp
                      - price_usd
                      properties:
                        uuid:
                          type: string
                        area_provinsi:
                          type: string
                        area_kota:
                          type: string
                        size:
                          type: string
                        price:
                          type: string
                        tgl_parsed:
                          type: string
                        timestamp:
                          type: string
                        price_usd:
                          type: string
                required:
                - status
                - meta
                - data
                x-examples:
                  example-1:
                    status: true
                    meta:
                      code: 200
                      errors:
                    data:
                    - uuid: ''
                      area_provinsi: ''
                      area_kota: ''
                      size: ''
                      price: ''
                      tgl_parsed: ''
                      timestamp: ''
                      price_usd: ''
              examples:
                example:
                  value:
                    status: true
                    meta:
                      code: 200
                      errors:
                    data:
                    - uuid: ''
                      area_provinsi: ''
                      area_kota: ''
                      size: ''
                      price: ''
                      tgl_parsed: ''
                      timestamp: ''
                      price_usd: ''
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors:
                        type: string
                        minLength: 1
                    required:
                    - code
                    - errors
                  data: {}
                required:
                - status
                - meta
                x-examples:
                  example-1:
                    status: false
                    meta:
                      code: 401
                      errors: Unauthorization
                    data:
              examples:
                example:
                  value:
                    status: false
                    meta:
                      code: 401
                      errors: Unauthorization
                    data:
      operationId: get-api-commodity
      description: ''
      security:
      - bearerAuth: []
    parameters: []
  "/api/comodity/admin":
    get:
      summary: Your GET endpoint
      tags: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors: {}
                    required:
                    - code
                  data:
                    type: array
                    uniqueItems: true
                    minItems: 1
                    items:
                      required:
                      - area_province
                      - max_profit
                      - min_profit
                      - average_profit
                      - median_profit
                      properties:
                        area_province:
                          type: string
                          minLength: 1
                        Profit:
                          type: object
                          properties:
                            Tahun 1:
                              type: object
                              properties:
                                Minggu ke 1:
                                  type: number
                              required:
                              - Minggu ke 1
                          required:
                          - Tahun 1
                        max_profit:
                          type: number
                        min_profit:
                          type: number
                        average_profit:
                          type: number
                        median_profit:
                          type: number
                required:
                - status
                - meta
                - data
                x-examples:
                  example-1:
                    status: true
                    meta:
                      code: 200
                      errors:
                    data:
                    - area_province: JAWA TIMUR
                      Profit:
                        Tahun 1:
                          Minggu ke 1: 100000
                      max_profit: 100000
                      min_profit: 100000
                      average_profit: 100000
                      median_profit: 100000
              examples:
                example:
                  value:
                    status: true
                    meta:
                      code: 200
                      errors:
                    data:
                    - area_province: JAWA TIMUR
                      Profit:
                        Tahun 1:
                          Minggu ke 1: 100000
                      max_profit: 100000
                      min_profit: 100000
                      average_profit: 100000
                      median_profit: 100000
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors:
                        type: string
                        minLength: 1
                    required:
                    - code
                    - errors
                  data: {}
                required:
                - status
                - meta
                x-examples:
                  example-1:
                    status: false
                    meta:
                      code: 401
                      errors: Unauthorization
                    data:
              examples:
                example:
                  value:
                    status: false
                    meta:
                      code: 401
                      errors: Unauthorization
                    data:
      operationId: get-api-comodity-admin
      security:
      - bearerAuth: []
components:
  schemas: {}
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
