package config

import (
"github.com/joho/godotenv"
"log"
"os"
)

var PORT, DATABASE_PATH, JWT_SECRET string
func init() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	PORT = os.Getenv("PORT")
	DATABASE_PATH = os.Getenv("DATABASE_PATH")
	JWT_SECRET = os.Getenv("JWT_SECRET")
}
