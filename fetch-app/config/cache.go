package config

import (
	"errors"
	"fetch-app/commodity"
	"time"
)

var RATE float64

type cache struct {
	repository commodity.Repository
}

func NewCache(repository commodity.Repository) *cache {
	return &cache{repository}
}

func (r *cache) StartWorker(ticker *time.Ticker) {
	r.UpdateCacheData()
	for {
		select {
		case <-ticker.C:
			r.UpdateCacheData()
		}
	}
}

func (r *cache) UpdateCacheData() {
	rate, err := r.repository.GetUsdRate()
	if err != nil {
		err = errors.New("Failed to get usd rate")
		return
	}

	RATE = rate
}