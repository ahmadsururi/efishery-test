package helper

import "time"

func StringToTime(timeString string) (time.Time, error) {
	formatString :=  "2008-01-01T10:14:15.000Z"
	res, err := time.Parse(formatString, timeString)

	if err != nil {
		layout := "2006-01-02T15:04:05-07:00"
		res, err := time.Parse(layout, timeString)
		if err != nil {
			formatString := "2008-01-01T10:14:15-0700"
			res, _ := time.Parse(layout, formatString)
			return res, nil
		}
		return res, nil
	}
	return res, nil
}

func CheckMaxProfit(data map[string]map[string]int) float64 {
	var max int
	for _, val := range data {
		for _, amount := range val {
			if amount >= max {
				max = amount
			}
		}
	}

	return float64(max)
}

func CheckMinProfit(data map[string]map[string]int) float64 {
	min := int(^uint(0) >> 1)
	for _, val := range data {
		for _, amount := range val {
			if amount <= min {
				min = amount
			}
		}
	}

	return float64(min)
}

func CheckAvgProfit(data map[string]map[string]int) float64 {
	var sum, counter int
	for _, val := range data {
		for _, amount := range val {
			sum += amount
			counter++
		}
	}

	return float64(sum / counter)
}

func CheckMedianProfit(data map[string]map[string]int) float64 {
	var arr []int
	for _, val := range data {
		for _, amount := range val {
			arr = append(arr, amount)
		}
	}

	counter := len(arr)

	if counter+1%2 == 0 {
		a := arr[(counter / 2)]
		b := arr[(counter/2)+1]
		return float64((a + b) / 2)
	} else {
		return float64(arr[counter/2])
	}
}