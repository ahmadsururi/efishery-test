package middleware

import (
	"fetch-app/config"
	"fetch-app/commodity"
	"fetch-app/helper"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"net/http"
	"strings"
)

func AuthMiddleware(role string) gin.HandlerFunc {
 	return func (c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if !strings.Contains(authHeader, "Bearer") {
			response := helper.ApiResponse(false, http.StatusUnauthorized, nil, "Unauthorization")
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		tokenString := ""
		arrayToken := strings.Split(authHeader, " ")
		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			return []byte(config.JWT_SECRET), nil
		})

		if err != nil {
			response := helper.ApiResponse(false, http.StatusUnauthorized, nil, "Unauthorization")
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		if !token.Valid {
			response := helper.ApiResponse(false, http.StatusUnauthorized, nil, "Unauthorization")
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		var claim commodity.UserFormatter
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			claim.Name = fmt.Sprintf("%v", claims["name"])
			claim.Role = fmt.Sprintf("%v", claims["role"])
			claim.Phone = fmt.Sprintf("%v", claims["phone"])
			claim.CreatedAt = fmt.Sprintf("%v", claims["created_at"])
		} else {
			response := helper.ApiResponse(false, http.StatusUnauthorized, nil, "Unauthorization")
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		if role != config.ALL_USER && role != claim.Role {
			response := helper.ApiResponse(false, http.StatusUnauthorized, nil, "Unauthorization")
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		c.Set("user", claim)
		c.Next()
	}
}