package main

import (
	"fetch-app/commodity"
	"fetch-app/config"
	"fetch-app/routers"
	"fmt"
	"github.com/gin-gonic/gin"
	"time"
)

func main() {
	router := gin.New()

	commodityRepository := commodity.NewRepository()
	cacheWorker := config.NewCache(commodityRepository)
	cacheTimer := time.NewTicker(time.Duration(60) * time.Minute)
	go cacheWorker.StartWorker(cacheTimer)

	api := router.Group("api")
	routers.ApiRouter(api)
	listenPort := fmt.Sprintf(":%s", config.PORT)
	fmt.Println("Running on port "+listenPort)
	router.Run(listenPort)
}