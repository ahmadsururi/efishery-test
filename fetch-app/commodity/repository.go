package commodity

import (
	"encoding/json"
	"errors"
	"net/http"
)

const COMMODITY_API_URL = "https://stein.efishery.com/v1/storages/5e1edf521073e315924ceab4/list"
const USD_RATE_URL = "https://free.currconv.com/api/v7/convert?q=IDR_USD&compact=ultra&apiKey=d6ae1141ff6716aa2860"

type Repository interface {
	getAllCommodity() ([]Commodity, error)
	GetUsdRate() (float64, error)
}

type repository struct {}

func NewRepository() *repository {
	return &repository{}
}

func (r *repository) getAllCommodity() ([]Commodity, error) {
	var commodityData []Commodity
	callCommodityApi, err := http.Get(COMMODITY_API_URL)
	if err != nil {
		return commodityData, errors.New("Failed to call commodity api")
	}
	defer callCommodityApi.Body.Close()

	err = json.NewDecoder(callCommodityApi.Body).Decode(&commodityData)
	if err != nil {
		return commodityData, errors.New("Failed to call commodity api")
	}

	return commodityData, nil
}

func (r *repository) GetUsdRate() (float64, error) {
	callUsdRate, err := http.Get(USD_RATE_URL)
	if err != nil {
		return 0, errors.New("Failed to call USD rate")
	}

	defer callUsdRate.Body.Close()

	var currencyRate CurrencyRate
	err = json.NewDecoder(callUsdRate.Body).Decode(&currencyRate)
	if err != nil {
		return 0, errors.New("Failed to call usd rate api")
	}

	if currencyRate.Rate == 0 {
		return 0, errors.New("Failed to call usd rate api")
	}

	return currencyRate.Rate, nil
}