package commodity

type Commodity struct {
	ID string `json:"uuid"`
	komoditas string `json:"komoditas"`
	AreaProvinsi string `json:"area_provinsi"`
	AreaKota string `json:"area_kota"`
	Size string `json:"size"`
	Price string `json:"price"`
	TanggalParsed string `json:"tgl_parsed"`
	Timestamp string `json:"timestamp"`
	USDPrice string `json:"price_usd"`
}

type CurrencyRate struct {
	Rate float64 `json:"IDR_USD"`
}