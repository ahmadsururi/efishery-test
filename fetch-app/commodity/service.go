package commodity

import (
	"fetch-app/helper"
	"fmt"
	"strconv"
)

type Service interface {
	GetAllCommodity() ([]Commodity, error)
	GetAdminComodities() ([]CommodityFormatter, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

type AreaFilter struct {
	province string
	amount   int
	year    string
	week   string
}

func (s *service) GetAllCommodity() ([]Commodity, error) {
	commodityData, err := s.repository.getAllCommodity()
	if err != nil {
		return commodityData, err
	}

	usdCommodityData, err := s.ConvertToUsdPrice(commodityData)
	if err != nil {
		return usdCommodityData, err
	}

	return usdCommodityData, nil
}

func (s *service) ConvertToUsdPrice(commodities []Commodity) ([]Commodity, error) {
	rate, err := s.repository.GetUsdRate()
	if err != nil {
		return commodities, err
	}

	var res []Commodity
	for _, commodity := range commodities {
		if commodity.Price == "" {
			res = append(res, commodity)
			continue
		}

		price, _ := strconv.ParseFloat(commodity.Price, 64)
		usd := price * rate
		commodity.USDPrice = fmt.Sprintf("$%f", usd)
		res = append(res, commodity)
	}

	return res, nil
}

func (s *service) GetAdminComodities() ([]CommodityFormatter, error) {
	var commodityFormatter []CommodityFormatter
	commodityData, err := s.repository.getAllCommodity()
	if err != nil {
		return commodityFormatter, err
	}

	var areaFilter []AreaFilter

	for _, commodity := range commodityData {
		if commodity.Price == "" ||
			commodity.TanggalParsed == "" ||
			commodity.Size == "" ||
			commodity.AreaProvinsi == "" {
			continue
		}

		dateTime, _ := helper.StringToTime(commodity.TanggalParsed)
		year, week := dateTime.ISOWeek()

		price, _ := strconv.Atoi(commodity.Price)
		size, _ := strconv.Atoi(commodity.Size)
		amount := price * size

		areaFilter = append(areaFilter, AreaFilter{
			province: commodity.AreaProvinsi,
			amount: amount,
			year: fmt.Sprintf("Tahun %s", strconv.Itoa(year)),
			week: fmt.Sprintf("Minggu ke %s", strconv.Itoa(week)),
		})
	}

	tempMap := make(map[string]map[string]map[string]int)
	for _, val := range areaFilter {
		if province, ok := tempMap[val.province]; !ok {
			week := map[string]int{val.week: val.amount}
			year := map[string]map[string]int{val.year: week}
			tempMap[val.province] = year
		} else {
			if year, ok := province[val.year]; !ok {
				week := map[string]int{val.week: val.amount}
				province[val.year] = week
			} else {
				if profit, ok := year[val.week]; !ok {
					year[val.week] = val.amount
				} else {
					year[val.week] += profit
				}
			}
		}
	}

	var result []CommodityFormatter
	for key, val := range tempMap {
		data := CommodityFormatter{
			AreaProvince: key,
			Profit: val,
			MinProfit: helper.CheckMinProfit(val),
			MaxProfit: helper.CheckMaxProfit(val),
			AvgProfit: helper.CheckAvgProfit(val),
			MedianProfit: helper.CheckMedianProfit(val),
		}

		result = append(result, data)
	}

	return result, nil
}