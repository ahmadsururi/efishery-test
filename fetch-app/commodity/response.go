package commodity

type UserFormatter struct {
	Name	   string `json:"name"`
	Role       string `json:"role"`
	Phone      string `json:"phone"`
	CreatedAt  string `json:"created_at"`
}

type CommodityFormatter struct {
	AreaProvince string `json:"area_province"`
	Profit map[string]map[string]int
	MaxProfit float64 `json:"max_profit"`
	MinProfit float64 `json:"min_profit"`
	AvgProfit float64 `json:"average_profit"`
	MedianProfit float64 `json:"median_profit"`
}