package routers

import (
	"fetch-app/commodity"
	"fetch-app/config"
	"fetch-app/handler"
	"fetch-app/middleware"
	"github.com/gin-gonic/gin"
)

func ApiRouter(e *gin.RouterGroup) *gin.RouterGroup {
	commodityRepository := commodity.NewRepository()
	commodityService := commodity.NewService(commodityRepository)
	commodityHandler := handler.NewCommodityHandler(commodityService)

	e.GET("/comodity", middleware.AuthMiddleware(config.ALL_USER), commodityHandler.GetList)
	e.GET("/comodity/admin", middleware.AuthMiddleware(config.ADMIN_ROLE), commodityHandler.GetAdminComodities)
	e.GET("/claims", middleware.AuthMiddleware(config.ALL_USER), commodityHandler.GetClaims)
	return e
}