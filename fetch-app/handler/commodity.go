package handler

import (
	"fetch-app/commodity"
	"fetch-app/helper"
	"github.com/gin-gonic/gin"
	"net/http"
)

type commodityHandler struct {
	service commodity.Service
}

func NewCommodityHandler(service commodity.Service) *commodityHandler {
	return &commodityHandler{service}
}

func (h *commodityHandler) GetList(c *gin.Context) {
	commodities, err := h.service.GetAllCommodity()
	if err != nil {
		response := helper.ApiResponse(false, http.StatusInternalServerError, nil, err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}

	response := helper.ApiResponse(true, http.StatusOK, commodities, nil)
	c.AbortWithStatusJSON(http.StatusOK, response)
	return
}

func (h *commodityHandler) GetAdminComodities(c *gin.Context) {
	commodities, err := h.service.GetAdminComodities()
	if err != nil {
		response := helper.ApiResponse(false, http.StatusInternalServerError, nil, err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}

	response := helper.ApiResponse(true, http.StatusOK, commodities, nil)
	c.AbortWithStatusJSON(http.StatusOK, response)
	return
}

func (h *commodityHandler) GetClaims(c *gin.Context) {
	currentUser, _ := c.Get("user")
	response := helper.ApiResponse(true, http.StatusOK, currentUser, nil)
	c.AbortWithStatusJSON(http.StatusOK, response)
	return
}