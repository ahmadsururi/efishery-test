module fetch-app

go 1.15

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/joho/godotenv v1.4.0
)
