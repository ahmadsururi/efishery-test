###Efishery Test

## Auth-App
`auth-app` create user and generate token

### Auth-App Setup
####1. Docker
```
$ cd auth-app
$ docker build -t <image-name> .
$ docker run -d -p <host port>:<docker port> <image-name>
```

example:
```
$ cd auth
$ docker build -t auth-app .
$ docker run -d -p 8080:8080 auth-app
```
####2. Local with `go1.15`
```
$ cd auth
$ go mod tidy
$ go run main.go
```

* `PORT` the application will listen (default: `8080`)
* `DATABASE_PATH` The file to store user data
* `JWT_SECRET` Secret key to use to generate/parse JWT

### Auth-App System Design
Using gorilla/mux
```
HTTP Request ---> Handler <---> Service <---> Repository <---> Database
```

## Fetch-App
`fetch-app` get commodity data from other API.

### Fetch-App Setup
####1. Docker
```
$ cd fetch-app
$ docker build -t <image-name> .
$ docker run -d -p <host port>:<docker port> <image-name>
```

example:
```
$ cd fetch-app
$ docker build -t fetch-app .
$ docker run -d -p 8080:8080 fetch-app
```
####2. Local with `go1.15`
```
$ cd fetch-app
$ go mod tidy
$ go run main.go
```

* `PORT` the application will listen (default: `8080`)
* `DATABASE_PATH` The file to store user data
* `JWT_SECRET` Secret key to use to generate/parse JWT

### Fetch-App System Design
Using gin-gonic golang framework
```
HTTP Request ---> Middleware ---> Handler <---> Service <---> Repository <---> Call Data
```

####API DOC LINK
[https://007.stoplight.io/docs/efishery/YXBpOjI2NjkyMjMy-auth-app](https://007.stoplight.io/docs/efishery/YXBpOjI2NjkyMjMy-auth-app)