package helper

import (
	"efishery-test/helper"
	"github.com/go-playground/assert"
	"testing"
)

func TestGeneratePassword(t *testing.T) {
	pass, _ := helper.GeneratePassword()
	assert.Equal(t, len(pass), 5)
}
