package helper

import (
	"efishery-test/helper"
	"github.com/go-playground/assert"
	"net/http"
	"testing"
)

func TestApiResponse(t *testing.T) {
	res := helper.ApiResponse(true, http.StatusOK, nil, nil)
	assert.Equal(t, res.Status, true)
}
