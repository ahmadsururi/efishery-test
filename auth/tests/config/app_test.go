package config

import (
	"github.com/joho/godotenv"
	"testing"
)

func TestGetEnv(t *testing.T) {
	if err := godotenv.Load("../../.env"); err != nil {
		t.Error("Error loading .env file")
	}
}