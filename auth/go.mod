module efishery-test

go 1.15

require (
	github.com/go-playground/assert v1.2.1
	github.com/go-playground/validator/v10 v10.9.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/sethvargo/go-password v0.2.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
