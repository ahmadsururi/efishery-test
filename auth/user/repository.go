package user

import (
	"efishery-test/config"
	"encoding/csv"
	"errors"
	"os"
)

type Repository interface {
	Store(user User) (User, error)
	GetAllUser() ([]User, error)
}

type repository struct {
}

func NewRepository() *repository {
	return &repository{}
}

func (r *repository) Store(user User) (User, error) {
	file, err := os.OpenFile(config.DATABASE_PATH, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return user, err
	}
	defer file.Close()

	csvWriter := csv.NewWriter(file)
	csvWriter.Write([]string{user.Name, user.Role, user.Phone, user.Password, user.CreatedAt})
	csvWriter.Flush()
	return user, nil
}

func (r *repository) GetAllUser() ([]User, error) {
	var users []User
	file, err := os.Open(config.DATABASE_PATH)
	if err != nil {
		return users, errors.New("Failed to open db file")
	}

	defer file.Close()
	userData, err := csv.NewReader(file).ReadAll()
	if err != nil {
		return users, errors.New("Failed to read db file")
	}

	for _, dataColumn := range userData {
		user := User{
			Name: dataColumn[0],
			Role: dataColumn[1],
			Phone: dataColumn[2],
			Password: dataColumn[3],
			CreatedAt: dataColumn[4],
		}

		users = append(users, user)
	}

	return users, nil
}