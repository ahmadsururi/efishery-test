package user

import (
	"efishery-test/config"
	"efishery-test/helper"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"time"
)

type Service interface {
	Register(input RegisterInput) (User, error)
	CheckUserAvailability(user User) (bool, error)
	GetToken(phone string, password string) (string, error)
	VerifyToken(userToken string) (UserFormatter, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) Register(input RegisterInput) (User, error) {
	user := User{}
	pwd, err := helper.GeneratePassword()
	if err != nil {
		return user, errors.New("Failed to generate password")
	}

	user.Name = input.Name
	user.Phone = input.Phone
	user.Role = input.Role
	user.Password = pwd
	user.CreatedAt = time.Now().Format("01 Nov 21 01:00 MST")

	isUserAvailable, err := s.CheckUserAvailability(user)
	if err != nil {
		return user, err
	}

	if isUserAvailable {
		return user, errors.New("Username not available")
	}

	newUser, err := s.repository.Store(user)
	if err != nil {
		return newUser, errors.New("Failed to create register user")
	}

	return newUser, nil
}

func (s *service) CheckUserAvailability(user User) (bool, error) {
	users, err := s.repository.GetAllUser()
	if err != nil {
		return true, err
	}

	for _, userData := range users {
		if userData.Name == user.Name {
			return true, nil
		}
	}

	return false, nil
}

func (s *service) Login(phone string, password string) (User, error) {
	users, err := s.repository.GetAllUser()
	var user User
	if err != nil {
		return user, errors.New("Failed to get users")
	}

	for _, userData := range users {
		if userData.Phone == phone && userData.Password == password {
			return userData, nil
		}
	}

	return user, errors.New("Failed to find user")
}

func (s *service) GetToken(phone string, password string) (string, error) {
	getUser, err := s.Login(phone, password)
	if err != nil {
		return "", err
	}

	token, err := s.GenerateToken(getUser)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (s *service) GenerateToken(user User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"name": user.Name,
		"role": user.Role,
		"phone": user.Phone,
		"created_at": user.CreatedAt,
	})

	tokenSigned, err := token.SignedString([]byte(config.JWT_SECRET))
	if err != nil {
		return "", errors.New("Failed to generate token")
	}

	return tokenSigned, nil
}

func (s *service) VerifyToken(userToken string) (UserFormatter, error) {
	token, err := jwt.Parse(userToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.JWT_SECRET), nil
	})

	var userFormatter UserFormatter
	if err != nil {
		fmt.Println(err)
		return userFormatter, errors.New("Failed to verify token")
	}

	if parseToken, isOk := token.Claims.(jwt.MapClaims); isOk && token.Valid {
		userFormatter.Name = fmt.Sprintf("%v", parseToken["name"])
		userFormatter.Role = fmt.Sprintf("%v", parseToken["role"])
		userFormatter.Phone = fmt.Sprintf("%v", parseToken["phone"])
		userFormatter.CreatedAt = fmt.Sprintf("%v", parseToken["created_at"])
	} else {
		fmt.Println("hola")
		return userFormatter, errors.New("Failed to verify token")
	}

	return userFormatter, nil
}