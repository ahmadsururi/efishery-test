package user

type UserFormatter struct {
	Name	   string `json:"name"`
	Role       string `json:"role"`
	Phone      string `json:"phone"`
	CreatedAt  string `json:"created_at"`
}