package user

type User struct {
	Name       string `json:"name"`
	Role       string `json:"role"`
	Phone      string `json:"phone"`
	Password   string `json:"password"`
	CreatedAt  string `json:"created_at"`
}