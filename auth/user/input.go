package user

type RegisterInput struct {
	Name string `json:"name" validate:"required"`
	Role string `json:"role" validate:"required"`
	Phone string `json:"phone" validate:"required"`
}

type LoginInput struct {
	Phone string `json:"phone" validate:"required"`
	Password string `json:"password" validate:"required"`
}