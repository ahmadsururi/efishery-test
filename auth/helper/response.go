package helper

type Response struct {
	Status bool `json:"status"`
	Meta Meta `json:"meta"`
	Data interface{} `json:"data"`
}

type Meta struct {
	Code int `json:"code"`
	Errors interface{} `json:"errors"`
}

func ApiResponse(
	status bool,
	code int,
	data interface{},
	err interface{},
	) Response {

	meta := Meta{
		Code: code,
		Errors: err,
	}

	return Response{
		Status: status,
		Meta: meta,
		Data: data,
	}
}