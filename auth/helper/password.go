package helper

import "github.com/sethvargo/go-password/password"

func GeneratePassword() (string, error) {
	pass, err := password.Generate(4, 1, 1, true, true)
	if err != nil {
		return "", err
	}

	return pass, nil
}