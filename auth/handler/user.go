package handler

import (
	"efishery-test/helper"
	"efishery-test/user"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"net/http"
	"strings"
)

type userHandler struct {
	userService user.Service
}

func NewUserHandler(userService user.Service) *userHandler {
	return &userHandler{userService}
}

func (h *userHandler) Register(w http.ResponseWriter, r *http.Request) {
	var input user.RegisterInput
	json.NewDecoder(r.Body).Decode(&input)
	validate := validator.New()
	err := validate.Struct(input)

	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		var errors []string
		for _, e := range err.(validator.ValidationErrors) {
			errors = append(errors, e.Error())
		}

		response := helper.ApiResponse(false, http.StatusBadRequest, nil, errors)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	newUser, err := h.userService.Register(input)
	if err != nil {
		response := helper.ApiResponse(false, http.StatusInternalServerError, nil, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(response)
		return
	}

	response := helper.ApiResponse(true, http.StatusOK, newUser.Password, nil)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
	return
}

func (h *userHandler) Token(w http.ResponseWriter, r *http.Request) {
	var input user.LoginInput
	json.NewDecoder(r.Body).Decode(&input)
	validate := validator.New()
	err := validate.Struct(input)

	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		var errors []string
		for _, e := range err.(validator.ValidationErrors) {
			errors = append(errors, e.Error())
		}

		response := helper.ApiResponse(false, http.StatusBadRequest, nil, errors)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	token, err := h.userService.GetToken(input.Phone, input.Password)
	if err != nil {
		response := helper.ApiResponse(false, http.StatusInternalServerError, nil, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(response)
		return
	}

	response := helper.ApiResponse(true, http.StatusOK, token, nil)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
	return
}

func (h *userHandler) GetClaimToken(w http.ResponseWriter, r *http.Request) {
	if len(r.Header["Authorization"]) == 0 {
		response := helper.ApiResponse(false, http.StatusInternalServerError, nil, "Internal server error")
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(response)
		return
	}

	authHeader := r.Header["Authorization"][0]
	if !strings.Contains(authHeader, "Bearer") {
		response := helper.ApiResponse(false, http.StatusInternalServerError, nil, "Internal server error")
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(response)
		return
	}

	tokenString := ""
	arrayToken := strings.Split(authHeader, " ")
	if len(arrayToken) == 2 {
		tokenString = arrayToken[1]
	}

	userData, err := h.userService.VerifyToken(tokenString)
	if err != nil {
		response := helper.ApiResponse(false, http.StatusInternalServerError, nil, "Failed to validate token")
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(response)
		return
	}

	response := helper.ApiResponse(true, http.StatusOK, userData, nil)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
	return
}