package main

import (
	"efishery-test/config"
	"efishery-test/routers"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	r := mux.NewRouter()
	routers.Api(r)

	listenPort := fmt.Sprintf(":%s", config.PORT)
	fmt.Println("Running on port "+listenPort)
	log.Fatal(http.ListenAndServe(listenPort, r))
}