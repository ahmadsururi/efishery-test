openapi: 3.1.0
info:
  title: AuthApp
  version: '1.0'
  summary: Efishery test
  description: Create auth api
servers:
  - url: http://localhost:8080
paths:
  /register:
    parameters: []
    post:
      summary: ''
      operationId: Register user
      responses:
        '200':
          description: Success register
          headers: {}
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    required:
                      - code
                    properties:
                      code:
                        type: number
                      errors: {}
                  data:
                    type: string
                    minLength: 1
                required:
                  - status
                  - meta
                  - data
              examples:
                example:
                  value:
                    status: true
                    meta:
                      code: 0
                      errors: null
                    data: string
            application/xml:
              schema:
                type: object
                properties: {}
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors:
                        type: array
                        items:
                          required: []
                          properties: {}
                    required:
                      - code
                      - errors
                  data: {}
                required:
                  - status
                  - meta
              examples: {}
      requestBody:
        content:
          application/json:
            schema:
              description: ''
              type: object
              properties:
                name:
                  type: string
                  minLength: 1
                phone:
                  type: string
                  minLength: 1
                role:
                  type: string
                  minLength: 1
              required:
                - name
                - phone
                - role
        description: ''
  /token:
    post:
      summary: ''
      operationId: get token
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors: {}
                    required:
                      - code
                  data:
                    type: string
                    minLength: 1
                required:
                  - status
                  - meta
                  - data
              examples:
                example:
                  value:
                    status: true
                    meta:
                      code: 200
                      errors: null
                    data: >-
                      eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVhdGVkX2F0IjoiMTEgTm92IDQxMSAxMTowMCBXSUIiLCJuYW1lIjoibWljaGVhbCIsInBob25lIjoiMDgxMzI3NzQ0NSIsInJvbGUiOiJ1c2VyIn0.Le3QnKIpnBw0nbqk5pYxcJTJc8AILa68xiY9lfhEzKs
        '500':
          description: Internal Server Error
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors:
                        type: string
                        minLength: 1
                    required:
                      - code
                      - errors
                  data: {}
                required:
                  - status
                  - meta
      description: Generate JWT token with valid phone number and password
      requestBody:
        content:
          application/json:
            schema:
              description: ''
              type: object
              properties:
                phone:
                  type: string
                  minLength: 1
                password:
                  type: string
                  minLength: 1
              required:
                - phone
                - password
            examples:
              example:
                value:
                  phone: '0813277445'
                  password: v!s2
        description: ''
  /claim:
    get:
      summary: Get Claim
      tags: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors: {}
                    required:
                      - code
                  data:
                    type: object
                    properties:
                      name:
                        type: string
                        minLength: 1
                      role:
                        type: string
                        minLength: 1
                      phone:
                        type: string
                        minLength: 1
                      created_at:
                        type: string
                        minLength: 1
                    required:
                      - name
                      - role
                      - phone
                      - created_at
                required:
                  - status
                  - meta
                  - data
              examples:
                example:
                  value:
                    status: true
                    meta:
                      code: 200
                      errors: null
                    data:
                      name: sururi
                      role: user
                      phone: '087839439'
                      created_at: 11 Nov 2021 11:00 WIB
        '500':
          description: Internal Server Error
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  status:
                    type: boolean
                  meta:
                    type: object
                    properties:
                      code:
                        type: number
                      errors:
                        type: string
                        minLength: 1
                    required:
                      - code
                      - errors
                  data: {}
                required:
                  - status
                  - meta
              examples:
                example:
                  value:
                    status: false
                    meta:
                      code: 500
                      errors: Failed to validate token
                    data: null
      operationId: get-claim
      parameters:
        - schema:
            type: string
          in: header
          name: Authorization
          description: Bearer token
      description: Get user data from JWT token
