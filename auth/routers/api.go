package routers

import (
	"efishery-test/handler"
	"efishery-test/user"
	"github.com/gorilla/mux"
)

func Api(r *mux.Router) {
	userRepository := user.NewRepository()
	userService := user.NewService(userRepository)
	userHandler := handler.NewUserHandler(userService)

	r.HandleFunc("/register", userHandler.Register).Methods("POST")
	r.HandleFunc("/token", userHandler.Token).Methods("POST")
	r.HandleFunc("/claim", userHandler.GetClaimToken).Methods("GET")
}